# NF-CAPSULE

This is a fork contains patches for the [capsule](https://github.com/puniverse/capsule) 
project which apparently is not maintained any more.


## Get started 

Compile: 

    make compile 

Test:

    make check

Upload distribution:

    make deploy

Release (stable) distribution: 

    make release

